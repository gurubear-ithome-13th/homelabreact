import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter } from "react-router-dom";
import Post from './post';
import PostDetail from './postdetail';
class Homelabroute extends Component {

    render() {
        return (
        <div>
            <Router>
                <Route exact path="/">
                    <Post/>
                </Route>
                <Route path="/details/:id"  render={(props) => (<PostDetail id={props.match.params.id}/>)} />
                    
            </Router>
        </div>
        )
    };
}


export default Homelabroute;
