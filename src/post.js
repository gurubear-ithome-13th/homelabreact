import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
// import PostDetail from './postdetail';

// import { Container, Table } from 'reactstrap';

class Post extends Component {

	// constructor(props) {
	// 	console.log(props);
	// 	super(props);
	state = { posts: [], term:'' };
	//}
	onFormSubmit = (event) => {
		event.preventDefault();
		//this.props.onSubmit(this.state.term);
	  };

	componentDidMount() {
		if(this.state.term){
			var queryurl = 'https://homelab.gurubear.info/API/Blog/GetList?tag='+this.state.term;
			fetch(queryurl).then(response => response.json())
				.then(data => this.setState({ posts: data }));
		}
		else{
			fetch('https://homelab.gurubear.info/API/Blog/GetList?').then(response => response.json())
			.then(data => this.setState({ posts: data }));
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if(this.state.term){
			var queryurl = 'https://homelab.gurubear.info/API/Blog/GetList?tag='+this.state.term;
			fetch(queryurl).then(response => response.json())
				.then(data => this.setState({ posts: data }));
		}
		else{
			fetch('https://homelab.gurubear.info/API/Blog/GetList?').then(response => response.json())
			.then(data => this.setState({ posts: data }));
		}
	}

	render() {
		const { posts } = this.state;

		const postsList = posts.map(post => {
			return <tr key={post.postID}>
                <td>{post.title}</td>
                <td>{post.createTime}</td>
                <td>{post.modifyTime}</td>
				<td>{post.tag}</td>
				{/* <td><Link to="/details">details</Link></td> */}
				<td><Link to={`/details/${post.postID}`}>Detail</Link></td>
			</tr>
		});

		return (
			<div> 
					<form onSubmit={this.onFormSubmit} className="ui form">
						<div className="field">
							<label>Tag Search</label>
							<input
							type="text"
							value={this.state.term}
							onChange={e => this.setState({ term: e.target.value })}
							/>
						</div>
					</form>
					<h3>清單</h3>
					<table className="mt-4">
						<thead>
							<tr>
								<th>標題</th>
								<th>建立時間</th>
								<th>修改時間</th>
								<th>標籤</th>
							</tr>
						</thead>
						<tbody>
							{postsList}
						</tbody>
					</table>
			</div>
		);
	}

}
export default Post;
