import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link ,useParams} from "react-router-dom";

class PostDetail extends Component {

    state = { title: '', content:'',tag:'',author:'',createTime: ''};
	componentDidMount() {
        var queryurl = 'https://homelab.gurubear.info/API/Blog/GetDetail?ID='+this.props.id;
        console.log(queryurl);
        fetch(queryurl).then(response => response.json())
            .then(data => this.setState({ 
                title: data.title,
                content:data.content,
                tag: data.tag,
                author: data.author,
                createTime: data.createTime
             }));
        console.log(this.state);
	}

    render() {
    //  const { postdata } = this.state;  
      return(
        <div>
          <h2>{this.state.title}</h2>
          <p>{this.state.author}</p>
          <p>{this.state.tag}</p>
          <p>{this.state.createTime}</p>
        </div>
      )
    }
  }

  export default PostDetail;
