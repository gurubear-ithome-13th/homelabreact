FROM node:14.17.6 As Builder
WORKDIR /homelab
COPY . .
RUN npm install
RUN npm run build

# using nginx hosting
FROM nginx:latest
COPY default.conf /etc/nginx/conf.d/
COPY --from=Builder /homelab/build /usr/share/nginx/html/